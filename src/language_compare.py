import sys, os
from datetime import datetime, timedelta
from storywrangling import Storywrangler
from storywrangling import Realtime
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.dates as mdates
import six
from google.cloud import translate_v2 as translate
import argparse
from bson import json_util
import json


def save_json(fname, tweets):
    """ Save tweets to disk as .json.gz"""
    with open(fname, 'wt') as f:
        json.dump(tweets, f, default=json_util.default, indent=2)


def load_json(fname):
    """ Load a gzipped json object of the tweets"""
    with open(fname, 'rt') as f:
        dict_list = json.load(f, object_hook=json_util.object_hook)
        return dict_list


def valid_date(s):
    try:
        return datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def text_reader(fname):
    with open(fname, 'r') as f:
        word_list = [i.strip() for i in f.readlines()]
    return word_list


def choose_langs(available_langs, args):
    """ Select list of language to plot and confirm they are available from Google"""
    langs = ['en',
             'es',
             'pt',
             'ar',
             'ko',
             'tr',
             'fr',
             'id',
             'uk',
             'it',
             'de',
             'ru',
             'fa',
             'pl',
             'tl',
             'nl',
             'ur',
             'ms',
             'ca',
             'el',
             'fi',
             'sv',
             'eo',
             'sr',
             'vi',
             ]
    print("Including:")
    print()

    lang_dict = {}
    index = 0
    for lang in langs:
        for available in available_langs:
            if lang == available['language']:
                index += 1
                print(f"{lang}: ", available['name'])
                lang_dict[lang] = available['name']

    print(f"\n{len(langs)} languages selected.")
    assert len(langs) == index, print("Some language codes don't match")

    save_json(args.lang_dict_fname, lang_dict)
    return langs, lang_dict


def get_lang_dict(args):
    lang_dict = load_json(args.lang_dict_fname)
    langs = [i for i in lang_dict.keys()]
    return langs, lang_dict


def translate_anchor(anchor, langs, lang_dict, translate_client, args):
    """ Get translations from Google for the provided anchor word for each language"""
    translations = []
    for lang in langs:

        # Run Google Translate Query
        try:
            translation_doc = translate_client.translate(anchor, target_language=lang, source_language='en')

            new_translation = {'lang': lang,
                               'translation': translation_doc['translatedText'],
                               'lang_name': lang_dict[lang],
                               'source_text': anchor,
                               'verified': False,
                               }
        except:
            new_translation = {'lang': 'en',
                               'translation': anchor,
                               'lang_name': lang_dict['en'],
                               'source_text': anchor,
                               'verified': False,
                               }
            pass

        translations.append(new_translation)


    print(translations)
    save_json(args.translations_fname, translations)
    return translations


def query_storywrangler(translations, anchor, start_date, end_date):
    """ Query each word from storywrangler databases in each langauge"""

    # initialize storywrangler client
    # You'll need access to the UVM network either through a VPN or port forwarding
    storywrangler = Storywrangler()

    queries = [(i['translation'].lower(), i['lang']) for i in translations]
    ngrams_array = storywrangler.get_ngrams_tuples(
        queries,
        start_time=start_date,
        end_time=end_date,
    )
    ngrams_array.to_csv(f'../data/{anchor}_timeseries.tsv', sep='\t')
    return ngrams_array


def query_storywrangler_realtime(translations, anchor, start_date, end_date):
    """ Query each word from storywrangler databases in each langauge"""

    # initialize storywrangler client
    # You'll need access to the UVM network either through a VPN or port forwarding
    realtime_langs = ['en', 'es', 'de', 'ar', 'fr', 'pt', 'ru', 'uk']

    storywrangler = Realtime()

    queries = [(i['translation'], i['lang']) for i in translations if i['lang'] in realtime_langs]
    ngrams_array = storywrangler.get_ngrams_tuples(
        queries,
    )
    ngrams_array.to_csv(f'../data/{anchor}_timeseries_realtime.tsv', sep='\t')
    return ngrams_array



def plot_compare_languages(ngrams_array, langs, lang_dict, anchor, args):
    """"""
    if args.realtime:
        f, axs = plt.subplots(4, 2, figsize=(8, 8), sharex=True, sharey=True)
        langs = ['en', 'es', 'de', 'ar', 'fr', 'pt', 'ru', 'uk']

    else:
        f, axs = plt.subplots(5, 5, figsize=(12, 8), sharex=True, sharey=True)
    axs = axs.ravel()
    print(ngrams_array)

    for i, lang in enumerate(langs):
        lang = lang_dict[lang]
        if lang == "Filipino":
            lang = 'Tagalog'
        data = ngrams_array.loc[:, :, lang]

        if args.plot_type == "freq":
            y_all = data.freq
            y_no_rt = data.freq_no_rt
            if i % 5 == 0:
                axs[i].set_ylabel('Usage Rate, $U$')
            #axs[i].set_ylim(10 ** (-6), 10 ** (-2))

        elif args.plot_type == "2021_index":
            prior_avg = np.mean(data.freq[:365])
            prior_avg_no_rt = np.mean(data.freq_no_rt[:365])
            y_all = data.freq / prior_avg
            y_no_rt = data.freq_no_rt / prior_avg_no_rt
            if i % 5 == 0:
                axs[i].set_ylabel('2021 Index, $I_{2021}$')
            plt.suptitle('Attention Index Normalized to 2021 Average')

        elif args.plot_type == "max_index":
            max_freq = np.max(data.freq)
            max_freq_no_rt = np.max(data.freq_no_rt)
            y_all = data.freq / max_freq
            y_no_rt = data.freq_no_rt / max_freq_no_rt
            if i % 5 == 0:
                axs[i].set_ylabel('Max Index, $\max_U$')
            plt.suptitle('Attention Index Normalized to Maximum')

        else:
            raise ValueError("Plot_type not valid, please choose from 'freq', '2021_index', and 'max_index'.")

        axs[i].semilogy(data.index.get_level_values('time'), y_all, label="All Tweets", alpha=0.75)
        axs[i].semilogy(data.index.get_level_values('time'), y_no_rt, label="No Retweets", alpha=0.75)
        if args.realtime:
            years = mdates.WeekdayLocator(1)  # every monday
            months = mdates.DayLocator()  #
            yearsFmt = mdates.DateFormatter('%b %d')
            if i == 1:
                axs[i].legend(bbox_to_anchor=(0.89, 1.6), frameon=False)
        else:
            if args.end_date - args.start_date < 700*timedelta(days=1):
                years = mdates.YearLocator()  # every year
                months = mdates.MonthLocator()  # every month
                yearsFmt = mdates.DateFormatter('%Y')
            else:
                years = mdates.YearLocator(3)  # every year
                months = mdates.MonthLocator(3)  # every month
                yearsFmt = mdates.DateFormatter('%Y')
            if i == 4:
                axs[i].legend(bbox_to_anchor=(0.89, 1.6), frameon=False)
        axs[i].xaxis.set_major_locator(years)
        axs[i].xaxis.set_major_formatter(yearsFmt)
        axs[i].xaxis.set_minor_locator(months)
        axs[i].grid()
        axs[i].set_title(f"{data.index.get_level_values('ngram')[0]} | {lang}")


    f.tight_layout()
    # plt.suptitle(anchor,y=1.0)
    plt.savefig(f"../figures/{anchor}_compare_language_attention_{args.plot_type}.png")
    plt.savefig(f"../figures/{anchor}_compare_language_attention_{args.plot_type}_clear.png", transparent=True)

    plt.savefig(f"../figures/{anchor}_compare_language_attention_{args.plot_type}.pdf")
    plt.show()

def parse_args(args):
    parser = argparse.ArgumentParser(
        description="A command line interface for creating interactive ambient tweet embedding plots",
    )
    parser.add_argument(
        '-w', '--word',
        type=str,
        help='anchor to plot'
    )
    parser.add_argument(
        '-l', '--lang_list',
        type=text_reader,
        help='textfile of words to plot'
    )
    parser.add_argument(
        '-r', '--realtime',
        help="Flag to get realtime data",
        action='store_true',
    )
    parser.add_argument(
        '-s', '--start_date',
        help="beginning of date range -- formate YYYY-MM-DD",
        type=valid_date,
        default='2021-01-01',
    )
    parser.add_argument(
        '-e', '--end_date',
        help="end of date range -- format YYYY-MM-DD",
        type=valid_date,
        default='2023-05-24'
    )
    parser.add_argument(
        '-p', '--plot_type',
        type=str,
        default='freq',
        help="Choose between 'freq' for frequency, "
             "'2021_index' for normalized to pre invasion attention levels,"
             " or 'max_index' for peak attention normalization."
    )

    return parser.parse_args(args)


def main(args=None):

    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)

    args.lang_dict_fname = f"../data/lang_dict_{args.word}.json"
    args.translations_fname = f"../data/translations_{args.word}.json"

    anchor = args.word
    start_date = args.start_date
    end_date = args.end_date

    # initialize google translate client
    # be sure to create a Google Cloud translation project and enable Cloud Translation:
    # https://cloud.google.com/translate/docs/setup
    if os.path.isfile(args.lang_dict_fname):
        langs, lang_dict = get_lang_dict(args)
        translations = load_json(args.translations_fname)
        print("Loaded translations from file.")
    else:
        translate_client = translate.Client()
        available_langs = translate_client.get_languages()

        langs, lang_dict = choose_langs(available_langs, args)

        #  translate
        translations = translate_anchor(anchor, langs, lang_dict, translate_client, args)
        print("Queried translations from Google API.")

    # grab attention timeseries from database
    if args.realtime:
        ngrams_array = query_storywrangler_realtime(translations, anchor, start_date, end_date)
    else:
        ngrams_array = query_storywrangler(translations, anchor, start_date, end_date)

    # plot -- Choose from {'freq', '2021_index', 'max_index'}
    plot_compare_languages(ngrams_array, langs, lang_dict, anchor, args)


if __name__ == "__main__":
    main()






